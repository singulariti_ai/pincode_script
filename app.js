    var request = require('request');
    var express = require('express');
    var app = express();
    var bodyParser = require('body-parser');
    var xlstojson = require("xls-to-json-lc");
    var xlsxtojson = require("xlsx-to-json-lc");
    const testFolder = 'file/';
    const fs = require('fs');
    var jsonfile = require('jsonfile')
    var pincodeJson = 'pincode.json';
    // app.use(bodyParser.json());
    var mongodb = require('mongodb');
    var MongoClient = mongodb.MongoClient;
    var mongoUrl = 'mongodb://localhost:27017/pincode';
    var pmongo = require('promised-mongo').compatible();
    var jsonexport = require('jsonexport');
    var exec = require('child_process').exec;
    var randomUseragent = require('random-useragent');
    var userAgent = randomUseragent.getRandom();

    // api for read pincode from xls and xlsx file 
    // and retrive lat long from geocoder api
    // then save into database "pincode" collection "pincode_lat_long_list".
    app.get('/', function(req, res) {

        MongoClient.connect(mongoUrl, function(err, db) {
            if (err) {
                throw err;
            }
            console.log('Database Connection established to', mongoUrl);

            var exceltojson;
            var keycount = 0;
            // file read from folder 
            fs.readdir(testFolder, (err, files, db) => {
                files.forEach(file => {
                    latlong(file);
                    console.log(file);
                });
            });

            function latlong(file) {

                if (file.split('.')[file.split('.').length - 1] === 'xlsx') {
                    exceltojson = xlsxtojson;
                } else {
                    exceltojson = xlstojson;
                }
                try {
                    exceltojson({
                        input: 'file/' + file,
                        output: null, //since we don't need output.json
                        lowerCaseHeaders: true
                    }, function(err, result) {
                        if (err) {
                            return res.json({
                                error_code: 1,
                                err_desc: err,
                                data: null
                            });
                        }
                        for (var i = 0; i < result.length; i++) {
                            var pincode = result[i]['pincode'];

                            geocoder(pincode); //lat long google api
                            if (i == result.length)
                                res.send(" done ");
                        }
                    });
                } catch (e) {
                    console.log({
                        error_code: 1,
                        err_desc: "Corupted excel file / not excel file"
                    });
                }
            }

            function geocoder(pincode) {

                if (pincode) {
                    if (keycount = 0) {
                        var key = '&key=AIzaSyAVjXfn0lFl1CA-K-hTj2Iz4b2xClZ_u7c';
                        keycount = 1;
                    } else {
                        var key = '&key=AIzaSyAVjXfn0lFl1CA-K-hTj2Iz4b2xClZ_u7c';
                        keycount = 0;
                    }
                    var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + pincode + key;


                    var pin_code = pincode.toString();
                    db.collection('pincode_lat_long_list').find({
                        "pincode": pin_code
                    }).count().then(
                        count => {

                            if (count == 0) {

                                var rq = request(url, function(error, response, body) {

                                    if (!error && response.statusCode == 200) {

                                        var reqbody = JSON.parse(body);
                                        // console.log(reqbody);
                                        if (reqbody.status == "OK") {

                                            // insert into data base when pincode does not exist in data base 
                                            db.collection('pincode_lat_long_list').insert({
                                                "pincode": pincode,
                                                "lat": reqbody.results[0].geometry.location.lat,
                                                "long": reqbody.results[0].geometry.location.lng
                                            });
                                            console.log("new pincode found ->" + pincode);
                                        } else {
                                            console.log({
                                                msg: "new pincode found",
                                                pincode: pincode,
                                                error: reqbody
                                            });
                                        }
                                    } else {

                                        console.log({
                                            error: error,
                                            pincode: pincode
                                        });

                                    }

                                });

                                rq.end();


                            } else {
                                // console.log(pincode + "<- found in data  base count is -> " + count);
                            }

                        },
                        err => console.log(err)
                    );

                }
            }
        });
    });

    // if pincode not available in data base
    // api = /pincode?pc=<your pincode>
    // it will  save pincode and lat_long into  database and throw response.
    app.get('/pincode', function(req, res) {

        MongoClient.connect(mongoUrl, function(err, db) {
            if (err) {
                throw err;
            }
            console.log('Database Connection established to', mongoUrl);

            var pincode = req.query.pc;
            var key = '&key=AIzaSyAVjXfn0lFl1CA-K-hTj2Iz4b2xClZ_u7c';
            console.log(key);
            var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + pincode + key;
            var pin_code = pincode.toString();

            db.collection('pincode_lat_long_list').find({
                "pincode": pin_code
            }).count().then(
                count => {

                    if (count == 0) {

                        request(url, function(error, response, body) {

                            if (!error && response.statusCode == 200) {

                                var reqbody = JSON.parse(body);
                                if (reqbody.status == "OK") {
                                    // insert into data base when pincode does not exist in data base 
                                    db.collection('pincode_lat_long_list').insert({
                                        "pincode": pincode,
                                        "lat": reqbody.results[0].geometry.location.lat,
                                        "long": reqbody.results[0].geometry.location.lng
                                    });

                                    res.send({
                                        error_code: 1,
                                        result: {
                                            pincode: pincode,
                                            lat: reqbody.results[0].geometry.location.lat,
                                            long: reqbody.results[0].geometry.location.lng
                                        }
                                    });
                                    console.log("new pincode found ->" + pincode);
                                } else {
                                    res.send({
                                        error_code: 0,
                                        error: reqbody
                                    });
                                }
                            } else {
                                res.send({
                                    error_code: 0,
                                    error: error,
                                });
                            }

                        });


                    } else {
                        console.log(pincode + "<- found in data  base count is -> " + count);

                        var data = db.collection('pincode_lat_long_list').find({ "pincode": pin_code });
                        data.each(function(err, doc) {
                            res.send({
                                error_code: 1,
                                result: doc
                            });

                        });

                    }

                },
                err => {
                    console.log(err)
                    res.send({
                        error_code: 0,
                        error: err
                    });
                }
            );

        });
    });

    // create json file 
    app.get('/json', function(req, res) {

        MongoClient.connect(mongoUrl, function(err, db) {
            if (err) {
                throw err;
            }
            console.log('Database Connection established to', mongoUrl);

            db.collection("pincode_lat_long_list").find().toArray().then(function(docs) {
                jsonfile.writeFile(pincodeJson, docs, function(err) {
                    if (err) console.error(err);
                    else res.send("file created ---> " + pincodeJson);
                });
            });

        });
    });

    // create csv file 
    app.get('/csv', function(req, res) {

        MongoClient.connect(mongoUrl, function(err, db) {
            if (err) {
                throw err;
            }
            console.log('Database Connection established to', mongoUrl);
            let command = 'mongoexport --csv -d pincode -c pincode_lat_long_list -f "pincode","lat","long" -o pincode.csv';

            exec(command, (err, stdout, stderr) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log(stdout);
                    console.log(stderr);
                }
            });


        });
    });

    app.listen('8080', function() {
        console.log('running on 8080...');
    });